# PMCV force field correction web
This is a containerized web application for [PMCV force field correction pipeline](https://gitlab.ics.muni.cz/467814/magicforcefield-pipeline).

## News

### v1.1
- added logging
- added development environment
- remoduled application
- increased k8s performance (removed throttling)

## Introduction
The application, implemented in Flask (Grinberg (2018)), performs the following tasks: it authenticates the user, looks up whether there is a running Jupyter notebook belonging to this user, spawns it if not, redirects the user to the notebook endpoint, and deletes the notebook deployment if it is not needed anymore.  The frontend is composed of the usual stack of Kubernetes components: Ingress to expose the frontend at a predefined public URL (including a preassigned DNS name and a TLS certificate), Service to manage the opened port and to link the Ingress to the deployment, and Deployment which contains the actual Docker container with the frontend application. The  mapping  between  user identities and the running notebooks is maintained in a trivial database which resides in another Kubernetes resource, Persistent Volume Claim (PVC) attached to the Deployment. The choice of user authentication  mechanism  iss  arbitrary, just a unique identification of the user is required. We prefer to use the ELIXIR authentication service (Linden et al. (2018)). Its registration process is lightweight and self-service and the user can sign up using institutional or social identity (Google, LinkedIn, ...). Sensitive information (e.g., credentials to use the authentication service or to create additional Kubernetes resources) is kept in a Kubernetes Secret.

