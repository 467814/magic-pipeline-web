#!/bin/bash
unset id
unset upload
default_name="spectraes/magic-pipeline-web"
default_tag="$(date +%F)${id}"
application="docker"
dockerfile="$PWD"

while getopts ":pi:tu" opt; do
  case $opt in
    p)
      application="podman"
      echo "building with podman.." >&2
      ;;
    i)
      id="-${OPTARG}"
      echo "building with id $id"
      ;;
	t)
	  dockerfile="${dockerfile}/development/Dockerfile"
	  default_name="localhost/magic-pipeline-web-test"
	  default_tag="latest"
	  echo "building with dockerfile ${dockerfile}"
	  ;;
	u)
	  upload="true"
	  echo "after build pushing image..."
	  ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done


name="${default_name}:${default_tag}${id}"
build="${application} build -t ${name} -f ${dockerfile} ."

eval "${build}"

if [ -n "${upload}" ]; then
	if [ "$dockerfile" = "$PWD" ]; then
		push="${application} push ${name}"
		echo "pushing image ${name}..." && eval "${push}"
	else
		echo "could not upload (e.g trying to push test image)" 1>&2
	fi
fi

