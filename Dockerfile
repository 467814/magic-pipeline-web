FROM ubuntu:latest

MAINTAINER Vladimir Visnovsky "467814@muni.cz"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
  && apt-get install -y python3-pip python3-dev \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip

COPY --from=lachlanevenson/k8s-kubectl:v1.20.2 /usr/local/bin/kubectl /usr/local/bin/kubectl
COPY app/ /app

RUN mkdir -p /root/.kube && ln -s /mnt/secret/config /root/.kube/config
RUN mkdir /var/lib/sqlite
WORKDIR /app

RUN pip3 install -r requirements.txt

ENTRYPOINT ["python"]
CMD ["/app/app.py"]

