### Development

There are files to be overriden in application for test purposes. These files serve to: 
- bypass authentication
- give admin full privileges in the container
- modify container behaviour to loop in sleep so that application can be terminated without need to restart container

For safety purposes there are steps needed to be taken manually after running in development mode:
- create *~/.kube* folder and create config file, fitting to k8s cluster in it
- change context so that you can access k8s cluster *kubectl config set-context --current --namespace=\<namespace>*
