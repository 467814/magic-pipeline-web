#!/bin/bash

id="latest"
application="docker"
container_name="pipeline-web"

while getopts ":pi:" opt; do
  case $opt in
    p)
      application="podman"
      echo "running with podman.." >&2
      ;;
    i)
      id="-${OPTARG}"
      echo "running with id $id"
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

name="localhost/magic-pipeline-web-test:${id}"



if $application ps | grep "$container_name" > /dev/null; then
    $application exec -it $container_name bash
else
    $application run -p 5000:5000 --name $container_name --entrypoint '/bin/bash' -d -ti $name -c 'while true; do sleep 10; done'
fi

