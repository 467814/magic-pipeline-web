import os
import time
import logging
import requests


from flask import redirect

log = logging.getLogger('__main__')


def check_running(user, jupyter_notebook):
    try:
        if requests.head(jupyter_notebook).status_code in [200, 405]:
            log.info('%s %s redirecting to existing notebook %s', user.id, user.name, jupyter_notebook)
            return redirect(jupyter_notebook)
    except requests.exceptions.ConnectionError:
        log.info('%s %s connection error to jupyter notebook %s', user.id, user.name, jupyter_notebook)

    log.info('%s %s allocating resources, dns name set (retry later)', user.id, user.name)
    return '<p>Taking longer to allocate needed resources... Please try to open pipeline from main page again later</p>'


def check_pvc_deleted(user):
    pvcs = os.popen('kubectl get pvc').read()
    user_pvc_name = f'pmcvff-correction-claim-{user.id}'
    if user_pvc_name in pvcs:
        log.info('%s %s pvc still getting deleted %s', user.id, user.name, pvcs)
        return '<p>Your personal storage is still getting deleted... Please try again later</p>'


def container_redirect(user, jupyter_notebook):
    timeout_threshold = 5
    while timeout_threshold >= 0:
        time.sleep(4)
        timeout_threshold -= 1
        try:
            response = requests.head(jupyter_notebook)
        except requests.exceptions.ConnectionError:
            log.info('%s %s connection error in new noteboook', user.id, user.name)
            continue
        if response.status_code in [200, 405]:
            log.info('%s %s redirecting successfully to new notebook %s', user.id, user.name, jupyter_notebook)
            return redirect(jupyter_notebook)
    log.info('%s %s still allocating resources for %s', user.id, user.name, jupyter_notebook)
    return '<p>Taking longer to allocate needed resources... Please try to open pipeline from main page again later</p>'


def remove_deployment(user_id):
    os.system(f'kubectl delete -f /srv/{user_id}')
    
