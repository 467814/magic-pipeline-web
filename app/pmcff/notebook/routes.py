import os
import sys
import time
import requests
import threading
import app
import logging
from flask import redirect, Blueprint, current_app, url_for
from flask_login import current_user

from pmcff.pipeline.pipeline_run import run_container
from pmcff.notebook import utils
from pmcff.user import User
from pmcff.db import get_connection

notebook = Blueprint('notebook', __name__)
log = logging.getLogger('__main__')


@notebook.route("/notebook")
def open_notebook():
    if current_user.is_authenticated:
        jupyter_notebook = f'https://{current_user.dns_name}/?token={current_user.token}'
        log.info('%s %s opening notebook', current_user.id, current_user.name)

        # Check if the service is running, redirect if so, else tell to wait
        if current_user.dns_name:
            return utils.check_running(current_user, jupyter_notebook)

        # Check if the pvc is deleted
        if out := utils.check_pvc_deleted(current_user):
            return out

        # Run pipeline container
        jupyter_notebook = run_container(current_user.id)
        log.info('%s %s running container', current_user.id, current_user.name)

        # Periodicaly wait until the container is available or timeout
        return utils.container_redirect(current_user, jupyter_notebook)
    return '<p>User not authorized to perform opening of notebook</p>'


@notebook.route("/delete")
def delete_notebook():
    if current_user.is_authenticated:
        connection = get_connection(current_app.config['DATABASE'])
        try:
            User.remove_container(connection, current_user.dns_name)
            log.info('%s %s successfully removed container %s', current_user.id, current_user.name, current_user.dns_name)
        except Exception as e:
            log.info('%s %s exception in delete container %s', current_user.id, current_user.name, e)
            return redirect(url_for("main.index"))
        threading.Thread(target=utils.remove_deployment, args=[current_user.id]).start()
        return redirect(url_for("main.index"))
    return '<p>User not authorized to remove notebook</p>'

