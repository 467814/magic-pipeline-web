import sys
import requests
import logging
import json

from flask import redirect, request, Blueprint, url_for, current_app
from flask_login import login_user, logout_user, login_required, current_user

import pmcff.login.utils as utils
from pmcff.db import get_connection
from pmcff.user import User
from pmcff import client

login = Blueprint('login', __name__)
log = logging.getLogger('__main__')


@login.route("/login")
def login_():
    provider_cfg = utils.get_provider_cfg()
    authorization_endpoint = provider_cfg["authorization_endpoint"]

    request_uri = client.prepare_request_uri(
        authorization_endpoint,
        redirect_uri=current_app.config['REDIRECT_URI'],
        scope=["openid", "email", "profile"],
    )
    return redirect(request_uri)


@login.route("/login/callback")
def callback():
    code = request.args.get("code")
    token_response = utils.get_response(code)

    if token_response.status_code == 400:
        return redirect(url_for("main.index"))
    client.parse_request_body_response(json.dumps(token_response.json()))

    provider_cfg = utils.get_provider_cfg()
    userinfo_endpoint = provider_cfg["userinfo_endpoint"]

    uri, headers, body = client.add_token(userinfo_endpoint)
    userinfo_response = requests.get(uri, headers=headers, data=body)

    # Get user info
    unique_id = userinfo_response.json()["sub"].split('@')[0]
    users_email = userinfo_response.json()["email"]
    users_name = userinfo_response.json()["given_name"]

    user = User(
        id_=unique_id, name=users_name, email=users_email, dns_name=None, token=None
    )

    # Doesn't exist? Add it to the internal database.
    connection = get_connection(current_app.config['DATABASE'])
    if not User.get(connection, unique_id):
        User.create(connection, id_=unique_id, name=users_name, email=users_email, dns_name=None, token=None)

    # Begin user session by logging the user in
    login_user(user)
    log.info('%s %s successfully logged in', user.id, user.name)

    # Send user back to homepage as authorized
    return redirect(url_for("main.index"))


@login.route("/logout")
@login_required
def logout_():
    log.info('%s %s logging out', current_user.id, current_user.name)
    logout_user()
    return redirect(url_for("main.index"))

