import os
import sys
import requests

from flask import current_app
from pmcff import login_manager
from pmcff.user import User
from pmcff.db import get_connection 

TEST_USER = User(
        id_="test123456", name="test_user", email="test_mail", dns_name=None, token=None
    )


@login_manager.user_loader
def load_user(user_id):
    connection = get_connection(current_app.config['DATABASE'])
    return User.get(connection, user_id)


def get_provider_cfg():
    return requests.get(current_app.config['DISCOVERY_URL']).json()


def remove_deployment(user_id):
    os.system(f'kubectl delete -f /srv/{user_id}')


def get_response(code):
    provider_cfg = get_provider_cfg()
    token_endpoint = provider_cfg["token_endpoint"]

    client_auth = requests.auth.HTTPBasicAuth(current_app.config['CLIENT_ID'], current_app.config['CLIENT_SECRET'])
    post_data = {"grant_type": "authorization_code",
                 "code": code,
                 "redirect_uri": current_app.config['REDIRECT_URI']}
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    response = requests.post(token_endpoint,
                             auth=client_auth,
                             headers=headers,
                             data=post_data)
    return response

