import os


class Config:

    # Auth
    CLIENT_ID = os.environ.get("CLIENT_ID", None)
    CLIENT_SECRET = os.environ.get("CLIENT_SECRET", None)
    DISCOVERY_URL = "https://login.elixir-czech.org/oidc/.well-known/openid-configuration"
    REDIRECT_URI = "https://pmcvff-correction.cerit-sc.cz/login/callback"

    # General
    DATABASE= "/var/lib/sqlite/sqlite_db"
    PIPELINE_IMAGE = "spectraes/pipeline:18-09-2021"
    GENERAL_NAME = "pmcvff-correction"
    TEMPLATES_PATH = f"/app/pmcff/pipeline/templates"
    YAMLS_PATH = "/srv"
    RUN_AS = 1001

    # Pvc
    PVC_NAME = f"{GENERAL_NAME}-claim"
    PVC_STORAGE = "10Gi"

    # Service
    SERVICE_NAME = f"{GENERAL_NAME}-svc"
    SERVICE_JUPYTERPORT = "jupyterport"

    # Ingress
    INGRESS_NAME = f"{GENERAL_NAME}-ingress"

