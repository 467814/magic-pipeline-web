from flask import render_template, Blueprint
from flask_login import current_user

main = Blueprint('main', __name__)


@main.route("/")
def index():
    if current_user.is_authenticated:
        return render_template('index_authorized.html',
                               user=current_user.name,
                               email=current_user.email,
                               dns_name=current_user.dns_name)
    else:
        return render_template('index_unauthorized.html')

