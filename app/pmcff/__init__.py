#!/usr/bin/env python3
import os
import sqlite3
import threading

# Third-party libraries
from flask import Flask
from flask_login import LoginManager
from oauthlib.oauth2 import WebApplicationClient
from waitress import serve

# Internal imports
from pmcff.db import init_db_command
from pmcff.config import Config

client = WebApplicationClient(Config.CLIENT_ID)
login_manager = LoginManager()


# Database setup
def initialize_db():
    try:
        init_db_command()
    except sqlite3.OperationalError:
        # Assume it's already been created
        pass


def create_app(config_class=Config):
    app = Flask(__name__)
    app.secret_key = os.environ.get("SECRET_KEY") or os.urandom(24)
    app.config.from_object(Config)

    login_manager.init_app(app)
    init_db = threading.Thread(target=initialize_db).start()

    from pmcff.main.routes import main
    from pmcff.notebook.routes import notebook
    from pmcff.login.routes import login
    app.register_blueprint(main)
    app.register_blueprint(notebook)
    app.register_blueprint(login)

    return app

