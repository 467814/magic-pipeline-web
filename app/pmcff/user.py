from flask_login import UserMixin
from pmcff.db import get_db


class User(UserMixin):
    def __init__(self, id_, name, email, dns_name, token):
        self.id = id_
        self.name = name
        self.email = email
        self.dns_name = dns_name
        self.token = token


    @staticmethod
    def get(conn, user_id):
        db = conn.cursor()
        
        user = db.execute(
            "SELECT * FROM user WHERE id = ?", (user_id,)
        ).fetchone()
        if not user:
            return None

        user = User(
            id_=user[0], name=user[1], email=user[2], dns_name=user[3], token=user[4]
        )
        return user


    @staticmethod
    def get_user_by_dns_name(conn, dns_name):
        db = conn.cursor()

        user = db.execute(
            "SELECT * FROM user WHERE dns_name = ?", (dns_name,)
        ).fetchone()
        if not user:
            return None

        user = User(
            id_=user[0], name=user[1], email=user[2], dns_name=user[3], token=user[4]
        )
        return user


    @staticmethod
    def remove_container(conn, dns_name):
        user = User.get_user_by_dns_name(conn, dns_name)
        if not user:
            raise Exception(f'Could not find user by dns_name: {dns_name}')
        user.dns_name = None
        user.token = None
        new_user = user

        User.update(conn, new_user)


    @staticmethod
    def assign_dns_name(conn, user_id, dns_name):
        db = conn.cursor()

        db.execute(
            "UPDATE user SET dns_name = ? WHERE id = ?",
            (dns_name, user_id)
        )

        conn.commit()


    @staticmethod
    def assign_token(conn, user_id, token):
        db = conn.cursor()

        db.execute(
            "UPDATE user SET token = ? WHERE id = ?",
            (token, user_id)
        )

        conn.commit()


    @staticmethod
    def update(conn, user):
        db = conn.cursor()

        db.execute(
            "UPDATE user SET name = ?, email = ?, dns_name = ?, token = ? WHERE id = ?",
                (user.name, user.email, user.dns_name, user.token, user.id)
        )

        conn.commit()


    @staticmethod
    def create(conn, id_, name, email, dns_name, token):
        db = conn.cursor()

        db.execute(
            "INSERT INTO user (id, name, email, dns_name, token) "
            "VALUES (?, ?, ?, ?, ?)",
            (id_, name, email, dns_name, token),
        )

        conn.commit()

