import sys
import os 
import secrets

from flask import current_app

from pmcff.pipeline import generators
from pmcff.user import User
from pmcff import db


def run_container(user_id):
    token = secrets.token_hex(24)
    dns_name = f"{user_id}.dyn.cerit-sc.cz"

    connection = db.get_connection(current_app.config['DATABASE'])
    User.assign_dns_name(connection, user_id, dns_name)
    User.assign_token(connection, user_id, token)

    if not os.path.exists(f"{current_app.config['YAMLS_PATH']}/{user_id}"):
        os.makedirs(f"{current_app.config['YAMLS_PATH']}/{user_id}")

    generators.ingress_gen(user_id, dns_name)
    generators.deployment_gen(user_id, token)
    generators.pvc_gen(user_id)
    generators.service_gen(user_id)

    os.system(f"kubectl apply -f {current_app.config['YAMLS_PATH']}/{user_id}")
    return f"https://{dns_name}/?token={token}"

