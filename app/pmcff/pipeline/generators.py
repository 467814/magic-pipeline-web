#!/usr/bin/env python3
import sys
import ruamel.yaml

from flask import current_app
from ruamel.yaml.scalarstring import DoubleQuotedScalarString


def deployment_gen(user_id, token):
    with open(f"{current_app.config['TEMPLATES_PATH']}/deployment-template.yaml") as ifile:
        doc = ruamel.yaml.round_trip_load(ifile, preserve_quotes=True)

        doc['metadata']['name'] = f"{current_app.config['GENERAL_NAME']}-{user_id}"
        doc['spec']['selector']['matchLabels']['app'] = f"{current_app.config['GENERAL_NAME']}-{user_id}"
        doc['spec']['template']['metadata']['labels']['app'] = f"{current_app.config['GENERAL_NAME']}-{user_id}"

        doc['spec']['template']['spec']['initContainers'][0]['image'] = current_app.config['PIPELINE_IMAGE']
        doc['spec']['template']['spec']['containers'][0]['image'] = current_app.config['PIPELINE_IMAGE']

        doc['spec']['template']['spec']['containers'][0]['securityContext']['runAsUser'] = current_app.config['RUN_AS']
        doc['spec']['template']['spec']['containers'][0]['securityContext']['runAsGroup'] = current_app.config['RUN_AS']

        pvc_name = f"{current_app.config['PVC_NAME']}-{user_id}"
        for env_var in doc['spec']['template']['spec']['containers'][0]['env']:
            if env_var['name'] == 'PVC_NAME':
                env_var['value'] = pvc_name
            if env_var['name'] == 'JUPYTER_TOKEN':
                env_var['value'] = token

        doc['spec']['template']['spec']['volumes'][0]['persistentVolumeClaim']['claimName'] = pvc_name

        ofile_name = f"deployment-user-{user_id}.yaml"
        with open(f"{current_app.config['YAMLS_PATH']}/{user_id}/{ofile_name}", "w") as ofile:
            ruamel.yaml.round_trip_dump(doc, ofile, explicit_start=True)


def ingress_gen(user_id, dns_name):
    with open(f"{current_app.config['TEMPLATES_PATH']}/ingress-template.yaml") as ifile:
        doc = ruamel.yaml.round_trip_load(ifile, preserve_quotes=True)

        doc['metadata']['name'] = f"{current_app.config['INGRESS_NAME']}-{user_id}"
        doc['spec']['tls'][0]['hosts'][0] = dns_name
        doc['spec']['tls'][0]['secretName'] = dns_name.replace('.', '-') + '-tls'
        doc['spec']['rules'][0]['host'] = dns_name
        doc['spec']['rules'][0]['http']['paths'][0]['backend']['service']['name'] = \
            f"{current_app.config['SERVICE_NAME']}-{user_id}"

        ofile_name = f"ingress-user-{user_id}.yaml"
        with open(f"{current_app.config['YAMLS_PATH']}/{user_id}/{ofile_name}", "w") as ofile:
            ruamel.yaml.round_trip_dump(doc, ofile, explicit_start=True)


def pvc_gen(user_id):
    with open(f"{current_app.config['TEMPLATES_PATH']}/pvc-template.yaml") as ifile:
        doc = ruamel.yaml.round_trip_load(ifile, preserve_quotes=True)

        doc['metadata']['name'] = f"{current_app.config['PVC_NAME']}-{user_id}"
        doc['spec']['resources']['requests']['storage'] = current_app.config['PVC_STORAGE']

        ofile_name = f"pvc-user-{user_id}.yaml"
        with open(f"{current_app.config['YAMLS_PATH']}/{user_id}/{ofile_name}", "w") as ofile:
            ruamel.yaml.round_trip_dump(doc, ofile, explicit_start=True)


def service_gen(user_id):
    with open(f"{current_app.config['TEMPLATES_PATH']}/service-template.yaml") as ifile:
        doc = ruamel.yaml.round_trip_load(ifile, preserve_quotes=True)

        doc['metadata']['name'] = f"{current_app.config['SERVICE_NAME']}-{user_id}"
        doc['metadata']['labels']['app'] = f"{current_app.config['GENERAL_NAME']}-{user_id}"

        doc['spec']['ports'][0]['name'] = f"{current_app.config['SERVICE_JUPYTERPORT']}-{user_id}"
        doc['spec']['selector']['app'] = f"{current_app.config['GENERAL_NAME']}-{user_id}"

        ofile_name = f"service-user-{user_id}.yaml"
        with open(f"{current_app.config['YAMLS_PATH']}/{user_id}/{ofile_name}", "w") as ofile:
            ruamel.yaml.round_trip_dump(doc, ofile, explicit_start=True)

