#!/usr/bin/env python3
from waitress import serve
from pmcff import create_app
import logging

app = create_app()

if __name__ == "__main__":
    logging.basicConfig(filename='debug.log', 
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')

    serve(app, host='0.0.0.0', port=5000)

